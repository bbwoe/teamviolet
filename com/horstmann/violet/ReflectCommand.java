package com.horstmann.violet;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.lang.reflect.Method;

import com.horstmann.violet.framework.Graph;

// Class taken from https://sourcemaking.com/design_patterns/command/java/2
// code in book: Design Patterns Explained Simply.
// Modified by Yukai

public class ReflectCommand implements Command{
    // the "encapsulated" object
    private Object receiver;    // likely can be changed to just TeamClassDiagramGraph 
    // the "pre-registered" request
    private Method action;
    // the "pre-registered" request
    private Object[] args;
        
    // Convenience Variables
    private CommandInfo info;
    private String receiverName;
    private String actionName;

//    public TeamClassDiagramGraph graph;
    public static int globalID;
    private final int ID;

    /**
     * Constructor 
     * @param graph -  that will call method -> obj.methodName(args)
     * @param methodName 
     * @param arguments
     */
    public ReflectCommand(Object o, String methodName, Object[] arguments) {
        
        // verbose
        //System.out.println("Constructor called for1 "+methodName);
        
        ID = globalID++;
        this.receiver = o;
        this.args = arguments;
        actionName = methodName;
        
        info = new CommandInfo(methodName,arguments);
        Class<? extends Object> cls = o.getClass();
        receiverName = cls.getSimpleName();
        
        Class[] argTypes = new Class[args.length];
        for (int i = 0; i < args.length; i++) {
            argTypes[i] = args[i].getClass();
            //System.out.println("arg types: "+argTypes[i].getSimpleName());
        }
        try {       
            action = getSuperMethod(cls, methodName,argTypes);
        } catch(NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Constructor using info object
     * @param graph
     * @param info
     */
    public ReflectCommand(TeamClassDiagramGraph graph, CommandInfo info){
        ID = globalID++;
        this.receiver = graph;
        this.args = info.getParameterTypes();
        actionName = info.getMethodName();
        this.info = info;
        Class<? extends Object> cls = graph.getClass();
        receiverName = cls.getSimpleName();
        Class[] argTypes = new Class[args.length];
        for (int i = 0; i < args.length; i++) {
            argTypes[i] = args[i].getClass();
        }
        try {       
            action = getSuperMethod(cls, actionName,argTypes);
        } catch(NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
    
    
    // Work around using: http://stackoverflow.com/questions/17953724/using-getdeclaredmethod-to-get-a-method-from-a-class-extending-another
    
    /**
     * helper function to check super classes for method
     * @param c
     * @param methodName
     * @param argTypes
     * @return
     * @throws NoSuchMethodException
     */
    private Method getSuperMethod(Class c, String methodName, Class [] argTypes) throws NoSuchMethodException{
        while(c!= null){
            Method[] methods = c.getDeclaredMethods();
            for (Method method : methods) {
                if (method.getName().equals(methodName)) {
                // this is a temoporary solution. need to compare argument types later
                //if (method.getName().equals(methodName) && method.getParameterTypes().equals(argTypes)) {
                    return method;
                }
            }
            c = c.getSuperclass(); 
        }
        return null;
    }

    /**
     * sends method name to server team class diagram to deal with
     */
    public void sendCommand(){
        ((TeamClassDiagramGraph) receiver).sendCommand(info);
    }

    /**
     * does the thing
     * @return
     */
    public void execute() {
        try {
            /* verbose tests
            System.out.println("execute called");
            System.out.println("receiverName: "+receiverName+"; methodName: "+actionName);
            System.out.println("Arugments as seen from execute(): ");
            for(int i=0; i< args.length;i++){
                System.out.println("arg "+i+": "+args[i].getClass().getCanonicalName());
            }
            */
            // dearth fix;
            if (actionName.equals("add")){
                //System.out.println(args[0].getClass().getSimpleName());
                if (args[0].getClass().getSimpleName().equals("ClassNode"))
                        args[0] = new ClassNode();
                if (args[0].getClass().getSimpleName().equals("InterfaceNode"))
                    args[0] = new InterfaceNode();
                if (args[0].getClass().getSimpleName().equals("PackageNode"))
                    args[0] = new PackageNode();
                if (args[0].getClass().getSimpleName().equals("NoteNode"))
                    args[0] = new NoteNode();
            }
            /* verbose tests
            Point2D m= (Point2D)args[1];
            System.out.println("m.getX: "+m.getX());
            System.out.println("m.getY: "+m.getY());
            */

            action.invoke(receiver, args);
            //return receiver.getClass().getMethod("getState").invoke(receiver);
        }
        //catch(IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
        catch(Exception e){
            e.printStackTrace();
        }
    }
    /**
     * 
     * @return
     */
    public String getActionName(){
        return actionName;
    }
    /**
     * 
     * @return
     */
    public String getReceiverName(){
        return receiverName;
    }
    /**
     * @return ID
     */
    public int getID(){
        return ID;
    }
    
}
