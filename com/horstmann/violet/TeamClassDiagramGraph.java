package com.horstmann.violet;

import com.horstmann.violet.ClassDiagramGraph;
import com.horstmann.violet.framework.Edge;
import com.horstmann.violet.framework.Graph;
import com.horstmann.violet.framework.Node;
import org.w3c.dom.Document;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Scanner;

public class TeamClassDiagramGraph extends ClassDiagramGraph {

   public static int globalDiagramID = 0;
   private String id;
   private int recentCommand= -1;

   public void setID(String id){
      this.id = id;
   }

   /**
    *
    * @param e
    * @param p1
    * @param p2
    * @return
    */
   public boolean connect(Edge e, Point2D p1, Point2D p2)
   {
      Node n1 = findNode(p1);
      Node n2 = findNode(p2);
      // if (n1 == n2) return false;
      return super.connect(e, p1, p2);
   }

   /*
    * (non-Javadoc)
    * @see com.horstmann.violet.ClassDiagramGraph#getNodePrototypes()
    */
   public Node[] getNodePrototypes()
   {
      return NODE_PROTOTYPES;
   }

   /*
    * (non-Javadoc)
    * @see com.horstmann.violet.ClassDiagramGraph#getEdgePrototypes()
    */
   public Edge[] getEdgePrototypes()
   {
      return EDGE_PROTOTYPES;
   }


   /**
    * updates most recent command
    */
   public void updateGraph(){
       //System.out.println("Calling updateGraph()");
       ServerConnection sc= new ServerConnection();
       ArrayList<CommandInfo> c = sc.receiveCommands(id);
       applyServerChanges(c);
   }
   /**
    * updates most recent command
    */
   private void update(int commandID){
       recentCommand = commandID;
   }
   /**
    * iterates through all the commands starting from the first unknown command
    * and executes them
    * @param commandInfo
    */
   public void applyServerChanges(ArrayList<CommandInfo> commandInfo){
       /* verbose testing
       System.out.println("CommandInfo Size: "+commandInfo.size());
       System.out.println("RecentCommandID: "+recentCommand);
       System.out.println("GlobalDiagramID: "+globalDiagramID);
       */
       if( recentCommand < commandInfo.size()-1){
           for (int i=(recentCommand+1); i < commandInfo.size(); i++){
               ReflectCommand c = new ReflectCommand(this, commandInfo.get(i));
               //System.out.println("receiver: "+c.getReceiverName()+"; action: "+c.getActionName());
               c.execute();
           }
           update(commandInfo.size()-1);
       }
   }

   /**
    * Sends a command information to the serverConnection
    * @param Command command
    */
   public void sendCommand(CommandInfo info){
       ServerConnection sc= new ServerConnection();
       sc.sendCommand(info,this.id);
   }
   /**
    *
    * @return id
    */
   public String getID() {
      return id;
   }

   private static final Node[] NODE_PROTOTYPES = new Node[4];

   private static final Edge[] EDGE_PROTOTYPES = new Edge[7];

   static
   {
      NODE_PROTOTYPES[0] = new ClassNode();
      NODE_PROTOTYPES[1] = new InterfaceNode();
      NODE_PROTOTYPES[2] = new PackageNode();
      NODE_PROTOTYPES[3] = new NoteNode();

      ClassRelationshipEdge dependency = new ClassRelationshipEdge();
      dependency.setLineStyle(LineStyle.DOTTED);
      dependency.setEndArrowHead(ArrowHead.V);
      EDGE_PROTOTYPES[0] = dependency;
      ClassRelationshipEdge inheritance = new ClassRelationshipEdge();
      inheritance.setBentStyle(BentStyle.VHV);
      inheritance.setEndArrowHead(ArrowHead.TRIANGLE);
      EDGE_PROTOTYPES[1] = inheritance;

      ClassRelationshipEdge interfaceInheritance = new ClassRelationshipEdge();
      interfaceInheritance.setBentStyle(BentStyle.VHV);
      interfaceInheritance.setLineStyle(LineStyle.DOTTED);
      interfaceInheritance.setEndArrowHead(ArrowHead.TRIANGLE);
      EDGE_PROTOTYPES[2] = interfaceInheritance;

      ClassRelationshipEdge association = new ClassRelationshipEdge();
      association.setBentStyle(BentStyle.HVH);
      association.setEndArrowHead(ArrowHead.V);
      EDGE_PROTOTYPES[3] = association;

      ClassRelationshipEdge aggregation = new ClassRelationshipEdge();
      aggregation.setBentStyle(BentStyle.HVH);
      aggregation.setStartArrowHead(ArrowHead.DIAMOND);
      EDGE_PROTOTYPES[4] = aggregation;

      ClassRelationshipEdge composition = new ClassRelationshipEdge();
      composition.setBentStyle(BentStyle.HVH);
      composition.setStartArrowHead(ArrowHead.BLACK_DIAMOND);
      EDGE_PROTOTYPES[5] = composition;

      EDGE_PROTOTYPES[6] = new NoteEdge();
   }
}
