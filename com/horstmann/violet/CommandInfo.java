package com.horstmann.violet;

import java.io.Serializable;

public class CommandInfo implements Serializable{
    private Object[] parameterTypes;
    private String methodName;
    
    public CommandInfo(String s, Object[] a){
        parameterTypes = a;
        methodName = s;
    }
    
    public Object[] getParameterTypes(){
        return parameterTypes;
    }
    public String getMethodName(){
        return methodName;
    }
}
