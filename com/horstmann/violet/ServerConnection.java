package com.horstmann.violet;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Scanner;

public class ServerConnection {

   private final String url = "http://138.197.220.63:9000";

   public ServerConnection(){
   }

   /**
    * opens conncetion and sends the command info.
    * also sets up the String to be sent
    * @param c
    * @param teamID
    */
   public void sendCommand(CommandInfo c, String teamID){
      try {
         String commandWithSlashes = serializeCommand(c);
         String commandString = commandWithSlashes.replaceAll("/", "SLASH");
         String query = teamID + "/" + commandString;

         HttpURLConnection connection = connect("/sendcmd/" + query);
         connection.getResponseMessage();

         InputStream response = new URL(url + "/sendcmd/" + query).openStream();
         try (Scanner scanner = new Scanner(response)) {
            String responseBody = scanner.useDelimiter("\\A").next();
            //System.out.println("Sending command (" + teamID + "): " + responseBody + "\nCommand string: " + commandString.substring(0,10) + " ....");
         }
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

   /**
    * receives the commands
    * deserializes the commands
    * @param id
    * @return
    */
   public ArrayList<CommandInfo> receiveCommands(String id){
      ArrayList<String> commandStrings = new ArrayList<>();
      ArrayList<CommandInfo> commands = new ArrayList<>();
      try {
         String query = id + "/-1";

         HttpURLConnection connection = this.connect("/getcmds/" + query);
         InputStream response = connection.getInputStream();
         URL url = new URL(this.url + "/getcmds/" + query);

         //System.out.println("Sending 'GET' request to URL : " + url
         //                  + " : " + connection.getResponseMessage());
         /*Put command strings into ArrayList*/
         BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
         String line = "";
         while((line = br.readLine()) != null){
            if(line.equals("END"))
               break;
            //System.out.println("Received commands:\n" + line.substring(0,10));
            commandStrings.add(line);
         }
         br.close();
      } catch (IOException e) {e.printStackTrace();}

      /* Deserialize the command strings*/
      for(int i = 0; i < commandStrings.size(); i++){
         commandStrings.set(i, commandStrings.get(i).replaceAll("SLASH", "/"));
         String s = commandStrings.get(i);
         if(!s.equals("ERROR_GROUPNOTFOUND"))
            try{
               //System.out.println("Command string: " + s.substring(0,10));
               commands.add(deserializeCommand(s));
            } catch (Exception e) {e.printStackTrace();}
      }
      return commands;
   }

   /**
    * creates a session
    * @param groupName
    * @return
    */
   private boolean createSession(String groupName){
      try {
         HttpURLConnection connection = connect("/checkexists/" + groupName);
         connection.getResponseMessage();

         if(!checkSessionExists(groupName)){
            connection = connect("/sendcmd/getsessions/" + groupName);
            connection.getResponseMessage();
            connection = connect("/createsession/" + groupName);
            connection.getResponseMessage();

            //System.out.println("\nCreating session at URL: " + url + "/createsession/" + groupName);

            return true;
         }
         else{
            connection.disconnect();
            return false;
         }

      } catch(IOException e) { e.printStackTrace(); }
      return false;
   }
   

   /**
    * checks if session exists
    * @param groupName
    * @return
    */
   private boolean checkSessionExists(String groupName){
      try {
         InputStream response = new URL(url + "/checkexists/" + groupName).openStream();
         try (Scanner scanner = new Scanner(response)) {
            String responseBody = scanner.useDelimiter("\\A").next();
            //System.out.println("checkSessionExists (" + groupName + "): " + responseBody);
            return responseBody.matches("true");
         }
      } catch(IOException e) { e.printStackTrace(); }
      return false;
   }

   private ArrayList<String> getSessions(){
      ArrayList<String> sessions = new ArrayList<>();
      try {
         URL url = new URL(this.url + "/grouplist/");
         BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
         String line = "";
         while ((line = br.readLine()) != null) {
            if (line.equals("END"))
               break;
            sessions.add(line);
         }
         br.close();
      } catch (Exception e) { e.printStackTrace(); }

      return sessions;
   }

   /**
    * 
    * @param query
    * @return
    * @throws MalformedURLException
    * @throws ProtocolException
    * @throws IOException
    */
   public HttpURLConnection connect(String query) throws MalformedURLException, ProtocolException, IOException{
      if(query.charAt(0) != '/') query = "/" + query;
      HttpURLConnection connection = (HttpURLConnection) new URL(url + query).openConnection();
      connection.setRequestMethod("GET");
      connection.setRequestProperty("Accept-Charset", StandardCharsets.UTF_8.name());
      return connection;
   }

   /**
    * serialize command
    * @param c
    * @return
    */
   private String serializeCommand(CommandInfo c){
      String byteCode = "";
      try {
         ByteArrayOutputStream baos = new ByteArrayOutputStream();
         ObjectOutputStream oos = new ObjectOutputStream(baos);
         oos.writeObject(c);
         oos.close();
         byteCode = Base64.getEncoder().encodeToString(baos.toByteArray());
      }
      catch(IOException e){ e.printStackTrace(); }
      return byteCode;
   }

   /**
    * deserialize
    * @param s
    * @return
    * @throws IOException
    * @throws ClassNotFoundException
    */
   private CommandInfo deserializeCommand(String s) throws IOException, ClassNotFoundException{
      byte [] data = Base64.getDecoder().decode( s );
      ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
      CommandInfo c  = (CommandInfo)ois.readObject();
      ois.close();
      return c;
   }


   /**
    * creates a popup dialog box when you initiate a Team Class Diagram Graph from the menus.
    * @param graph
    * @param t
    * @return
    */
   public boolean showConnectionMenu(TeamClassDiagramGraph graph, Timer t){
      JFrame frame;
      JPanel panel;
      JButton okButton;
      JButton createNewDiagramButton;
      JLabel label;
      JTextArea textArea;
      JComboBox<String> comboBox;

      ArrayList<String> sessionList = getSessions();
      String[] sessions = sessionList.toArray(new String[sessionList.size()]);
      comboBox = new JComboBox<>(sessions);
      comboBox.setEditable(true);


      frame = new JFrame("Connect");
      frame.setSize(500,200);
      frame.setLocationRelativeTo(null);
      frame.setVisible(true);
      frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

      panel = new JPanel();
      panel.setBackground(Color.WHITE);

      createNewDiagramButton = new JButton("Create New Session");
      label = new JLabel("Enter Team Diagram Name:");
      textArea = new JTextArea(1, 20);
      textArea.setBackground(Color.LIGHT_GRAY);
      okButton = new JButton("OK");

      okButton.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            String text = (String)comboBox.getSelectedItem();
            if(text.matches("[\\w]+") && checkSessionExists(text)){
               graph.setID(text);
               t.start();
               //updateGraphTimer(graph);
               //return true;
               frame.dispose();
            }
            else{
               comboBox.setBackground(new Color(255, 165, 165)); //Light red
            }
         }
      });

      createNewDiagramButton.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            String text = (String)comboBox.getSelectedItem();
            boolean success = createSession(text);
            if(success) {
               graph.setID(text);
               t.start();
               //updateGraphTimer(graph);
               frame.dispose();
            }
            else {
               comboBox.setBackground(new Color(255, 165, 165)); //Light red
            }
         }
      });

      panel.add(label);
      panel.add(comboBox);
      panel.add(okButton);
      panel.add(createNewDiagramButton);

      frame.add(panel);
      return true;
   }

   /**
    * 
    * @param graph
    */
   public void updateGraphTimer(TeamClassDiagramGraph graph){
      int delay = 500;
      Timer t = new Timer(delay, (ActionEvent e) -> {
        // System.out.println("\nTimer is running: " + delay/1000 + "s intervals.");
         graph.updateGraph();
      });
      t.start();
   }

   /**
    * 
    * @return
    */
   public String getUrl() {
      return url;
   }
}
