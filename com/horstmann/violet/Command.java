package com.horstmann.violet;

import java.io.Serializable;

public interface Command extends Serializable{
    void execute();
//    User getUser();
    int getID();
}
