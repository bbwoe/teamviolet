package com.horstmann.violet;

import java.io.Serializable;

/**
 * Created by Brandt on 5/10/2017.
 */
public class TestCommand implements Command, Serializable {
    public TestCommand(TeamClassDiagramGraph graph){
        ID = globalID++;
        this.graph = graph;
    }
    public void execute(){

    }

    public int getID() {
        return ID;
    }

    public TeamClassDiagramGraph graph;
    public static int globalID = 0;
    private final int ID;
}
